## How to Use, Setup and Run


### 1. Ensure $YOUR_DOMAIN.COM is a public domain. Letsencrypt needs to be able to reach your public domain for validations. $YOUR_DOMAIN.COM must have an "A"(Address) record. The ‘A’ record specifies the IP address (IPv4) of a host. Whenever DNS servers get a query to resolve $YOUR_DOMAIN.COM, it will refer the A record to answer the IP address.

#### How to check and verify public DNS Record of $YOUR_DOMAIN.COM

 For Example on Linux you can use the 'dig' command to check for "nginx-docker.dremer10.com.
```
dig nginx-docker.dremer10.com

; <<>> DiG 9.11.3-1ubuntu1.13-Ubuntu <<>> nginx-docker.dremer10.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 3735
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 65494
;; QUESTION SECTION:
;nginx-docker.dremer10.com.     IN      A

;; ANSWER SECTION:
nginx-docker.dremer10.com. 0    IN      A       34.xx.xxx.xxx

;; Query time: 0 msec
;; SERVER: 127.0.0.53#53(127.0.0.53)
;; WHEN: Sun Oct 18 20:23:02 PDT 2020
;; MSG SIZE  rcvd: 70
```

The first two lines tell us the version of dig (9.11.3), the command line parameters (nginx-docker.dremer10.com) and the query options (printcmd). The printcmd option means that the command section (the name given to these first two lines) is printed. You can turn it off by using the option +nocmd.

Next, dig shows the header of the response it received from the DNS server. Here it reports that an answer was obtained from the query response (opcode: QUERY) and that the response contains one answer, two pieces of information in the authority section, and a further two in the additional section. The flags are used to note certain things about the DNS server and its response; for example, the RA flag shows that recursive queries are available.

Next comes the question section, which simply tells us the query, which in this case is a query for the A record of nginx-docker.dremer10.com. The IN means this is an Internet lookup (in the Internet class).

The answer section tells us that nginx-docker.dremer10.com has the IP address 34.xx.xxx.xxx.

Along with the IP address the DNS record contains some other useful information. The authority section contains a list of name servers that are responsible for the domain name — those that can always give an authoritative answer. Here we find two name servers listed, which are the name servers of the company with which the domain was registered. To save an extra lookup, dig lists the IP addresses of those name servers in the additional section.

Lastly there are some stats about the query. You can turn off these stats using the +nostats option.

By default dig is quite verbose. One way to cut down the output is to use the +short option:

```
dig nginx-docker.dremer10.com +short
```
which will drastically cut the output to:
```
34.xx.xxx.xxx
```


### 2. Next you will have some files you will need to update
   - In the "docker-compose.yml" -- add your email address and the DOMAIN you wish to add SSL to.
   - In the "conf.d/default.conf" -- add <YOUR_PUBLIC_DOMAIN.COM> as noted in .conf file. Leave the "https" section commented out until later instructed.



### 3. Run docker-compose for 1st time. 

This will create a cerbot container and an nginx container. The nginx container will start a server listening on port 80 as defined in the "default.conf" file. It will be listening for requests to the domain you entered from earlier. The Certbot container will create a privkey.pem and the certificate signing request(.csr) for your domian. This will be validated by letsencrypt Certificate Authority and they give you a certificate in return that they signed using their root certificate(CA) and private key. All browsers have a copy (or access a copy from the operating system) of Letsencrypt root certificate, so the browser can verify that your certificate was signed by a trusted CA.. This is why the domain must be registered and publically accessible (in this case by our nginx container with http access to our domain). 


```
docker-compose --verbose up -d

Creating network "nginx-ssl_default" with the default driver
Creating nginx-ssl_web_1	... done
Creating nginx-ssl_cerbot_1	... done

```

### 4. Check the status of containers
If everything runs correctly you should see the certbot container Exit with a 0 [ ok ] then move on to Step 5.
If you see Exit 1 then the request likely failed due to a connection request to your domain. Look through the verbose output for a clue to what exactly went wrong.


```
docker-compose ps

	Name		Command				State		Ports
-------------------------------------------------------------------------------
nginx-ssl_cerbot_1 certbot certonly --webroot    ...	Up	443/tcp, 80/tcp
nginx-ssl-web_1    nginx -g daemon off;			Up	0.0.0.0->443/tcp, 0.0.0.0:80->80/tcp


docker-compose ps

        Name            Command                         State           Ports
-------------------------------------------------------------------------------
nginx-ssl_cerbot_1 certbot certonly --webroot    ...    Exit 0
nginx-ssl-web_1    nginx -g daemon off;                 Up      0.0.0.0->443/tcp, 0.0.0.0:80->80/tcp


```


### 5. Verify that you now have the generated certs. 
On your Host machine, verify the certs were created in "cerbot/conf/live/<YOUR_PUBLIC_DOMAIN.COM>/" The directory should now contain 4 certs
(cert.pem,chain.pem,fullchain.pem, and privkey.pem). Now you will need to edit the "conf.d/default.conf" file again and this time UNCOMMENT the entire https section.
Bring the containers down. This is to ensure our server will now use the newly UNCOMMENT HTTPS section of our nginx default.conf file.

```
docker-compose down

Stopping nginx-ssl_web_1     ... done
Removing nginx-ssl_web_1     ... done
Rmoving nginx-ssl_certbot_1  ... done
Removing network nginx-ssl_default
```

### 6. Bring the containers back up. Now our nginx server will contain our newly generated SSL certs and will be listening on https port 443.
```
docker-compose --verbose up -d
Creating network "nginx-ssl_default" with the default driver
Creating nginx-ssl_web_1        ... done
Creating nginx-ssl_cerbot_1     ... done
```

### 7. Verify the Nginx Server is Up and Running
```
docker-compose ps web

        Name            Command                         State           Ports
-------------------------------------------------------------------------------
nginx-ssl-web_1    nginx -g daemon off;                 Up      0.0.0.0->443/tcp, 0.0.0.0:80->80/tcp


# Check the Service is LISTENING on 443

netstat -pltn


```



### 8. Open a web browser and Navigate to https://YOUR_PUBLIC_DOMAIN.COM. You should resolve the website and have the padlock indicating the site is using https and is secure.


# To Renew Certificate

#### bring nginx container down by running the following in the project root dir.
```
docker-compose down
```
#### ensure you are only listening on port 80 (http) based off of the default80.conf file.
```
cat conf.d/default80.conf > conf.d/default.conf
```
#### Bring up the container to renew certs
```
docker-compose up
``` 
#### This will output exactly what is happening so you can see any errors that occur. If all is successful you should see the following output with "Congratulations" 
```
web_1      | 3.122.178.200 - - [22/May/2021:15:36:52 +0000] "GET /.well-known/acme-challenge/KUTgXL62XzV76GkmVAWECnM2o0LXQACxlRzHv3hIRso HTTP/1.1" 200 87 "-" "Mozilla/5.0 (compatible; Let's Encrypt validation server; +https://www.letsencrypt.org)" "-"
web_1      | 54.189.22.122 - - [22/May/2021:15:36:52 +0000] "GET /.well-known/acme-challenge/KUTgXL62XzV76GkmVAWECnM2o0LXQACxlRzHv3hIRso HTTP/1.1" 200 87 "-" "Mozilla/5.0 (compatible; Let's Encrypt validation server; +https://www.letsencrypt.org)" "-"
certbot_1  | Cleaning up challenges
certbot_1  | IMPORTANT NOTES:
certbot_1  |  - Congratulations! Your certificate and chain have been saved at:
certbot_1  |    /etc/letsencrypt/live/proxy.dremer10.com/fullchain.pem
certbot_1  |    Your key file has been saved at:
certbot_1  |    /etc/letsencrypt/live/proxy.dremer10.com/privkey.pem
certbot_1  |    Your cert will expire on 2021-08-20. To obtain a new or tweaked
certbot_1  |    version of this certificate in the future, simply run certbot
certbot_1  |    again. To non-interactively renew *all* of your certificates, run
certbot_1  |    "certbot renew"
certbot_1  |  - If you like Certbot, please consider supporting our work by:
certbot_1  |
certbot_1  |    Donating to ISRG / Let's Encrypt:   https://letsencrypt.org/donate
certbot_1  |    Donating to EFF:                    https://eff.org/donate-le
certbot_1  |
nginx-ssl-letsencrypted_certbot_1 exited with code 0
```
### Once you obtain the renewed cert use the default443.conf to ensure you are once again listening on port 443 (https)
```
cat conf.d/default443.conf > conf.d/default.conf
```

#### Now bring up the conatiners in "detached" mode
```
docker-compose up -d
```

#### Verify your site is now running with SSL by navigating to https://YOUR_PUBLIC_DOMAIN.com
